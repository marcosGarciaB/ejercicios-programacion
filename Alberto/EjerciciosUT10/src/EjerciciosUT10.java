/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author alumnot
 */
import java.io.*;
import java.util.*;

public class EjerciciosUT10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
         * Ejercicios apartado 1.
         * Ejercicio 1.
         * 
         * Muestra todos los archivos de una carpeta junto con su tamaño. Métodos
         * relevantes: isDirectory, listFiles, isFile, getName, length (en bytes).
         */
        // File miCarpeta = new File("../../Documentos/");
        // String[] nombreArchivos = miCarpeta.list();
        // String[] nombres = {"Aaaa", "Bbbb", "Cccc", "Dddd", "Eeee", "Ffff", "Gggg",
        // "Hhhh"};
        // File[] archivos = new File[8];
        // long[] tamano = new long[miCarpeta.list().length];
        //
        // for (int i = 0; i < nombreArchivos.length; i++) {
        // archivos[i] = new File(miCarpeta.getAbsolutePath());
        // }
        //
        // if (miCarpeta != null) {
        // for (String c: nombreArchivos) {
        // System.out.println("Nombre del archivo - " + c);
        // }
        // for (long t: tamano) {
        // t = archivos.length;
        // System.out.println("Tamaño del archivo - " + t);
        // }
        // }
        // Ejercicio 2.
        //
        // Lista todos los archivos de un árbol de directorios tiene que entrar en
        // tantas carpetas como tenga el árbol (similar a lo que hace el comando tree en
        // linux).
        //
        //
        // Ejercicio 3.
        //
        // Explora la clase File en el API de Java e identifica diferentes excepciones
        // generadas por algunos de sus métodos.
        //
        //
        // Ejercicio 4.
        //
        // Busca en la API de Java información sobre java.nio y familiarízate con
        // algunas de las diferencias que tiene esta librería sobre java.io.
        // Ejercicios apartado 2.
        // Ejercicio 3.
        //
        // Crea un programa que escriba cinco números aleatorios del 1 al 9 en cada
        // línea,
        // luego lea todos los números del archivo y añada a cada línea el valor de los
        // números sumados hasta dicha línea.
        // No debes crear variables intermedias que guarden los números, debes escribir
        // el archivo y después leer y escribir el archivo de nuevo.
        // Ejemplo de ejecución, contenido del archivo:
        //
        // 3 (suma:3)
        //
        // 5 (suma:8)
        //
        // 2 (suma:10)
        //
        // 7 (suma:17)
        //
        // 1 (suma:18)
        try {
            PrintWriter pw = new PrintWriter(new FileWriter("aleatorios.txt"));
            FileReader fr = new FileReader("aleatorios.txt");
            for (int i = 0; i < 8; i++) {
                pw.write((int) Math.random() * 9 + 1);
                int c = fr.read();
                System.out.println("Contenido del archivo:" + c);
                while (c != -1) {
                    c = fr.read();
                    System.out.println((char) c);
                }
            }
            pw.close();

        } catch (Exception e) {
            System.out.println("Error en la escritura del archivo. " + e.getMessage());

        }

        // Ejercicios apartado 3.
        // Ejercicio 1.
        //
        // Enlace a El Quijote de Cervantes en Gutenberg library:
        //
        // https://www.gutenberg.org/cache/epub/2000/pg2000.txt
        //
        // Realiza una lista de las 100 palabras más repetidas en el libro. Para contar
        // las palabras usa la colección o colecciones óptimas y optimiza la lectura del
        // fichero.
        //
        // Nota: Puedes suponer que las palabras junto a signos de puntuación o acentos
        // son diferentes, por ejemplo, que y ¿que pueden ser palabras diferentes, como
        // y cómo pueden ser diferentes.
        try (BufferedReader quijote = new BufferedReader(new FileReader("Quijote.txt"))) {
            String linea;
            StringBuilder historia = new StringBuilder();
            while ((linea = quijote.readLine()) != null) {
                historia.append(linea).append(" ");
            }
            String[] palabras = historia.toString().split("\\s+");

            Map<String, Integer> contadorPalabras = new HashMap<>();
            for (String palabra : palabras) {
                contadorPalabras.put(palabra, contadorPalabras.getOrDefault(palabra, 0) + 1);
            }

            for (Map.Entry<String, Integer> entry : contadorPalabras.entrySet()) {
                System.out.println("Palabra: " + entry.getKey() + " repetida " + entry.getValue() + " veces");
            }
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Error de E/S: " + e.getMessage());
        }

    }
    /*
     * Ejercicios apartado 4.
     * Ejercicio 1.
     * 
     * Crea un archivo binario con la información de 50 Pokémon, de forma aleatoria.
     * Ten en cuenta que la información que debes almacenar es: Número de Pokémon
     * (ten en cuenta que en la actualidad hay 1025 Pokemon, desde 1-Bulbasaur hasta
     * 1025-Pecharunt), el género, y las siguientes estadísticas (hp, ataque,
     * defensa, ataque especial, defensa especial y velocidad).
     * 
     * Teniendo en cuenta que las estadísticas pueden tomar valores del 20 al 260,
     * usa los tipos de datos que minimicen el tamaño del fichero.
     * 
     * Luego lee el contenido del fichero y vuelta los datos a un fichero en formato
     * texto con la información de la siguiente forma para cada Pokémon:
     ***********************************************
     * 
     * 
     * Número: 123 *
     * 
     * Género: Hembra *
     * 
     * Estadísticas: *
     * 
     * HP (054) [---- ] *
     * 
     * Ataque (189) [--------------- ] *
     * 
     * Defensa (085) [------- ] *
     * 
     * Ataque esp. (154) [------------ ] *
     * 
     * Defensa esp. (214) [----------------- ] *
     * 
     * Velocidad (078) [------ ] *
     ***********************************************
     * 
     * 
     * Entre corchetes deben aparecer guiones proporcionalmente al valor de las
     * estadística, si el valor fuese 260 tendría los 20 guiones, y si el valor es
     * 20 tendría un guión.
     * 
     * 
     */
}
