
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeSet;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author alumnot
 */
public class EjerciciosUT09 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
         * Ejercicios apartado 2.
         * Ejercicio 1.
         * 
         * Crea un método estático que pueda calcular la nota media a partir de las
         * notas pasadas en un ArrayList.
         * Las clases usadas para inicializar el ArrayList pueden ser Integer, Float o
         * Double.
         * 
         */
        System.out.println("----Ejercicio 1 apartado 2---- \n");
        ArrayList<Double> notas = new ArrayList<>(10);
        insertarNotas(notas);
        calcularMedia(notas);

        /*
         * Ejercicios apartado 3.
         * Ejercicio 1.
         * 
         * Crea un pokédex (solo de 10 pokemon, no hace falta que sean todos). Luego
         * genera 20 capturar aleatorias de pokemons,
         * al final del programa se debe mostrar el estado de la pokédex.
         */
        System.out.println("\n\n\n");
        System.out.println("----Ejercicio 1 apartado 3---- \n");

        HashMap<Integer, String> pokedex = new HashMap<>();
        Pokemon bulbasaur = new Pokemon("Bulbasaur", 1);
        Pokemon ivysaur = new Pokemon("Ivysaur", 2);
        Pokemon venusaur = new Pokemon("Venusaur", 3);
        Pokemon charmander = new Pokemon("Charmander", 4);
        Pokemon charmeleon = new Pokemon("Charmeleon", 5);
        Pokemon charizard = new Pokemon("Charizard", 6);
        Pokemon squirtle = new Pokemon("Squirtle", 7);
        Pokemon wartortle = new Pokemon("Wartortle", 8);
        Pokemon blastoise = new Pokemon("Blastoise", 9);
        Pokemon caterpie = new Pokemon("Caterpie", 10);
        Pokemon[] grupo = { bulbasaur, ivysaur, venusaur, charmander, charmeleon, charizard, squirtle, wartortle,
                blastoise, caterpie };
        // capturarPokemon(grupo, pokedex);
        randomNum();
        abecedario();
    }

    /*
     * public static void capturarPokemon(Pokemon[] grupo, HashMap<Integer, String>
     * pokedex) {
     * try {
     * int aux = 0;
     * int probabilidadCaptura = 0;
     * boolean intentos = false;
     * while (intentos == false) {
     * for (int i = 1; i <= 20; i++) {
     * aux = (int) (Math.random() * 9 + 1);
     * System.out.println("[Intento de captura número " + (i) + "]");
     * System.out.println("Ha aparecido un " + grupo[aux].getNombre() +
     * " salvaje!");
     * Thread.sleep(2000);
     * System.out.println("Capturando Pokemon...");
     * probabilidadCaptura = (int) (Math.random() * 3 + 1);
     * Thread.sleep(4000);
     * if (probabilidadCaptura == 3 || probabilidadCaptura == 2) {
     * System.out.println("Capturando Pokemon...");
     * probabilidadCaptura = (int) (Math.random() * 2 + 1);
     * Thread.sleep(4000);
     * if (probabilidadCaptura == 1 || probabilidadCaptura == 2) {
     * if (!pokedex.containsValue(grupo[aux].getNombre())) {
     * pokedex.put(i, grupo[aux].getNombre());
     * }
     * System.out.println("Se ha capturado un " + pokedex.get(i));
     * } else if (probabilidadCaptura == 2) {
     * System.out.println("Captura fallida!!!");
     * }
     * } else {
     * System.out.println("El Pokémon se ha escapado!!!");
     * }
     * if (i == 20) {
     * intentos = true;
     * System.out.println("\t\t\t----Información de la Pokedex----");
     * for (Map.Entry<Integer, String> entrada : pokedex.entrySet()) {
     * int numero = entrada.getKey();
     * String nombre = entrada.getValue();
     * System.out.println("Pokémon nº " + numero + ": " + nombre);
     * }
     * }
     * }
     * }
     * } catch (Exception e) {
     * System.out.println("Se ha producido un error" + e.getMessage());
     * }
     * }
     */
    public static void insertarNotas(ArrayList<? super Double> notas) {
        notas.add(Math.random() * 10 + 1);
        notas.add(Math.random() * 10 + 1);
        notas.add(Math.random() * 10 + 1);
        notas.add(Math.random() * 10 + 1);
        notas.add(Math.random() * 10 + 1);
    }

    public static double calcularMedia(ArrayList<? extends Number> notas) {
        double suma = 0;
        for (Number nota : notas) {
            suma += nota.doubleValue(); // Convertimos cada number en double.
        }
        double media = suma / notas.size();
        media = Math.round(media * 100.0) / 100.0; // Redondeamos a dos decimales.
        System.out.println("Tu nota media es de: " + media);
        return media;
    }

    /*
     * Ejercicios apartado 4.
     * Ejercicio 2.
     * 
     * Crea un programa que genere 20 números aleatorios del 1 al 1000, muéstrales
     * por pantalla.
     * Con un iterador muestra todos los números impares.
     * Con otro iterador elimina todos los números múltiplos de tres, diciendo por
     * pantalla qué números han sido eliminados y,
     * finalmente, muestra la lista de números resultantes.
     */

    public static void randomNum() {
        System.out.println("----Ejercicio 2 apartado 4---- \n");
        List<Integer> aleatorio = new ArrayList<>();
        Random rdm = new Random();
        for (int i = 0; i < 20; i++) {
            aleatorio.add(rdm.nextInt(1000) + 1);
        }
        Iterator impar = aleatorio.iterator();
        Iterator imparEliminado = aleatorio.iterator();

        while (impar.hasNext()) {
            int imp = (int) impar.next();
            if (imp % 2 != 0) {
                System.out.println("Números impares: " + imp);
            }
        }

        while (imparEliminado.hasNext()) {
            int imp = (int) imparEliminado.next();
            if (imp % 3 == 0) {
                imparEliminado.remove();
                System.out.println("Eliminado el número: " + imp);
            }
        }
        System.out.println(aleatorio);

    }

    /*
     * Ejercicio 3.
     * 
     * Crea una lista de 30 palabras de forma aleatoria. Cada palabra se formará por
     * tres letras del abecedario al azar.
     * Muestra la lista por pantalla. Usando un iterador, recorre una segunda vez la
     * lista y las palabras que contengan alguna vocal deben convertirse en
     * mayúsculas.
     * Muestra la lista tras la conversión.
     * Ejemplo:
     * rfg
     * hrw
     * GHU
     * ACE
     * hjl
     */

    public static void abecedario() {
        LinkedList<Character> abecedario = new LinkedList<>();
        abecedario.add('a');
        abecedario.add('b');
        abecedario.add('c');
        abecedario.add('d');
        abecedario.add('e');
        abecedario.add('f');
        abecedario.add('g');
        abecedario.add('h');
        abecedario.add('i');
        abecedario.add('j');
        abecedario.add('k');
        abecedario.add('l');
        abecedario.add('m');
        abecedario.add('n');
        abecedario.add('ñ');
        abecedario.add('o');
        abecedario.add('p');
        abecedario.add('q');
        abecedario.add('r');
        abecedario.add('s');
        abecedario.add('t');
        abecedario.add('u');
        abecedario.add('v');
        abecedario.add('w');
        abecedario.add('x');
        abecedario.add('y');
        abecedario.add('z');
        Random rdm = new Random();
        List<String> palabras = new ArrayList<>();
        String auxString = "";
        for (int i = 0; i < 30; i++) {
            for (int j = 0; j < 3; j++) {
                int aux = rdm.nextInt(26) + 1;
                auxString += String.valueOf(abecedario.get(aux));
            }
            palabras.add(auxString);
            auxString = "";
        }
        System.out.println(palabras);
        Iterator<String> itt = palabras.iterator();
        String[] vocales = { String.valueOf(abecedario.get(0)), String.valueOf(abecedario.get(4)),
                String.valueOf(abecedario.get(8)), String.valueOf(abecedario.get(15)),
                String.valueOf(abecedario.get(21)) };

        List<String> palabrasModificadas = new ArrayList<>(); // Nueva lista para almacenar las palabras modificadas

        while (itt.hasNext()) {
            String palabra = itt.next();
            boolean contieneVocal = false;
            for (String vocal : vocales) {
                if (palabra.contains(vocal)) {
                    contieneVocal = true;
                    break; // No necesitamos continuar si ya hemos encontrado una vocal
                }
            }
            if (contieneVocal) {
                palabrasModificadas.add(palabra.toUpperCase()); // Agregar la palabra en mayúsculas a la lista de
                                                                // palabras modificadas
            } else {
                palabrasModificadas.add(palabra); // Agregar la palabra original a la lista de palabras modificadas
            }
        }

        palabras.clear(); // Limpiar la lista original
        palabras.addAll(palabrasModificadas); // Agregar todas las palabras (modificadas y no modificadas) de vuelta a
                                              // la lista original
        itt = palabras.iterator(); // Si no se vuelve a hacer el iterador, la siguiente ejecución no mostrará los
                                   // cambios
        while (itt.hasNext()) {
            String palabra = itt.next();
            System.out.println(palabra);
        }

        /*
         * Ejercicios apartado 6.
         * Ejercicio 1.
         * Crea un programa con un hashMap e introduce algunos datos.
         * Cuando el usuario quiera introducir un nuevo elemento el programa debe
         * informar si ese elemento ya existe y comunicárselo al usuario,
         * si no existe debe añadirlo y comunicar al usuario que el elemento ha sido
         * añadido.
         */

        try {
            HashMap<Integer, String> datos = new HashMap<>();
            datos.put(1, "Dato 1");
            datos.put(2, "Dato 2");
            datos.put(3, "Dato 3");
            datos.put(4, "Dato 4");
            datos.put(5, "Dato 5");
            int aux = 0;
            for (int i = 0; i < 3; i++) {
                Scanner sc = new Scanner(System.in);
                System.out.println("Introduce un dato: ");
                String entrada = sc.nextLine();

                if (datos.containsValue(entrada)) {
                    throw new IllegalArgumentException("Error: no se ha podido añadir este elemento porque ya existe");
                } else {
                    for (Map.Entry<Integer, String> d : datos.entrySet()) {
                        aux = d.getKey();
                    }
                    datos.put(aux + 1, entrada);
                    System.out.println("Dato agregado correctamente.");
                    System.out.println("Datos actuales: " + datos);
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getLocalizedMessage());
        }

        /*
         * Ejercicio 2.
         * Escribe un programa que simule el comportamiento de una impresora.
         * La impresora tiene una cola de impresión que almacenará documentos a
         * imprimir.
         * Si la cola llega a 20 elementos no se podrán añadir más documentos y debe
         * generar una excepción.
         */

        try {
            Queue<Integer> impresora = new LinkedList<>();
            boolean llena = false;
            Random rndm = new Random();
            Random porcentaje = new Random();
            int aux = 0;
            int aux2 = 0;
            while (llena != true) {
                for (int i = 1; i <= 20; i++) {
                    if (impresora.size() == 20) {
                        llena = true;
                        throw new Exception("ERROR: No se pueden añadir más de 20 documentos a la cola.");
                    } else {
                        impresora.add(rndm.nextInt(1000) + 1);
                        System.out.println("Agregado elemento número " + i + " a la cola. Huecos restantes en la cola: "
                                + (20 - impresora.size()));
                        aux++;
                        if (rndm.nextInt(3) + 1 == 2 || rndm.nextInt(3) + 1 == 3) {
                            aux2++;
                            System.out.println("Imprimiendo elemento número " + aux2 + ": " + impresora.poll());
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            // TODO: handle exception
        }
    }
}
