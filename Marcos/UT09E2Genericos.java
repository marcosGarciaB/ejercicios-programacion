 
package ut09e2genericos;

import java.util.ArrayList;
import ut09e2genericos.animales.*;


public class UT09E2Genericos {

    public static void main(String[] args) {
        
        // usando clases muy similares sin genéricos
        System.out.println("----- Usando clases muy similares sin genéricos -----");
        InfoBoolean ib = new InfoBoolean(Boolean.TRUE);
        ib.dameInfo();
        InfoInteger ii = new InfoInteger(234);
        ii.dameInfo();
        InfoString is = new InfoString("Hola");
        is.dameInfo();
        
        System.out.println("");
        System.out.println("");
        
        // mismo ejemplo con genéricos
        System.out.println("----- Usando genéricos en nuestras clases -----");
        Info<Boolean> ib2 = new Info<>(Boolean.FALSE);
        ib2.dameInfo();  
        Info<Integer> ii2 = new Info<>(7);     
        ii2.dameInfo();
        Info<String> is2 = new Info<>("Adiós");
        is2.dameInfo();
        Info<Double> id = new Info<>(8.4);
        id.dameInfo();
        
        // Genéricos es lo que hemos usado para definir colecciones como ArrayList
        ArrayList<String> al1 = new ArrayList<>();
        al1.add("lunes");
        al1.add("martes");
        al1.add("miércoles");
        
        // jerarquía de parámetros
        CuidarAnimal<Animal> ca1 = new CuidarAnimal<>(new Animal());
        CuidarAnimal<Perro> ca2 = new CuidarAnimal<>(new Perro());
        CuidarAnimal<Gato> ca3 = new CuidarAnimal<>(new Gato());
        CuidarAnimal<Gato> ca4 = new CuidarAnimal<>(new Gato());
        //CuidarAnimal<String> ca4 = new CuidarAnimal<>("Animal falso"); // <- error
        
        System.out.println(ca4.toString());
        
        System.out.println("");
        System.out.println("");
        
        // métodos genéricos
        System.out.println("----- Usando métodos con genéricos -----");
        miMetodo("cosa");
        miMetodo(3);
        miMetodo(true);
        
        System.out.println("");
        System.out.println("");
        
        // ? wildcard
        System.out.println("----- Usando métodos con wildcard ? -----");
        miMetodoWildcard(al1);
        //miMetodoWildcardAnimales(al1); // <- error
        
        ArrayList<Integer> al2 = new ArrayList<>();
        al2.add(1);
        al2.add(2);
        al2.add(3);
        
        miMetodoWildcard(al2);
        
        ArrayList<Animal> ala = new ArrayList<>();
        ala.add(new Animal());
        
        ArrayList<Perro> alp = new ArrayList<>();
        alp.add(new Perro());
        alp.add(new Perro());
        
        miMetodoWildcardAnimales(ala);
        miMetodoWildcardAnimales(alp);
        
        System.out.println("");
        System.out.println("");
        System.out.println("");
        
        System.out.println("\t\t---- Ejercicio 1 ----");
        /*
        Crea un método estático que pueda calcular la nota media a partir de las notas
        pasadas en un ArrayList. Las clases usadas para inicializar el ArrayList 
        pueden ser Integer, Float o Double.
        */
        
        ArrayList<Integer> notasInt = new ArrayList<>();
        notasInt.add(8);
        notasInt.add(7);
        notasInt.add(7);
        notasInt.add(8);
        notasInt.add(9);
        
        ArrayList<Float> notasFloat = new ArrayList<>();
        notasFloat.add(8.3f);
        notasFloat.add(7.1f);
        notasFloat.add(7.6f);
        notasFloat.add(8.8f);
        notasFloat.add(9.6f);
        
        ArrayList<Double> notasDouble = new ArrayList<>();
        notasDouble.add(9.0);
        notasDouble.add(9.8);
        notasDouble.add(9.9);
        notasDouble.add(9.2);
        notasDouble.add(9.4);
        
        calcularMedia(notasInt);
        calcularMedia(notasFloat);
        calcularMedia(notasDouble);
        
        
    }
    
    public static <T> void miMetodo(T dato){
        System.out.println("miMetodo: " + dato);
    }
    
    public static void miMetodoWildcard(ArrayList<?> lista){
        System.out.println("Lista: " + lista);
    }
    
    public static void miMetodoWildcardAnimales(ArrayList<? extends Animal> lista){
        System.out.println("Lista de animales: " + lista.size());
    }
    
    public static <T extends Number> void calcularMedia(ArrayList<T> notas) {
        System.out.println("Notas: " + notas);
        
        double total = 0.0;
        
        for (T nota : notas) {
            total += nota.doubleValue();
        }
        
        double media = total / notas.size();
        System.out.println("Media: " + media);
    }
   
    
    /*
Ejercicio 2.
    Investiga cómo puedes hacer que una clase se parametrice con una clase y dos
    interfaces.
    */
}
