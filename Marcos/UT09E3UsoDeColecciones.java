package ut09e3usodecolecciones;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;
import java.util.Vector;


public class UT09E3UsoDeColecciones {

    public static void main(String[] args) {
         System.out.println("\t\t---- Ejercicio 1 ----");
        /*
        Crea un pokédex (solo de 10 pokemon, no hace falta que sean todos). Luego genera
        20 capturas aleatorias de pokemons, al final del programa se debe mostrar el
        estado de la pokédex.
        */
        
        //TreeMAp supongo
         
        System.out.println("");
        System.out.println("");
        System.out.println("");
         
        System.out.println("\t\t---- Ejercicio 2 ----");
        /*
        Crea un pequeño diccionario inglés-español con algunas palabras. El usuario
        podrá poner una palabra en inglés por consola y el programa responderá con
        la palabra en español si está en la lista o con un mensaje diciendo que esa
        palabra no se encuentra en el diccionario.
        */
        Scanner lector  = new Scanner(System.in);
        
        HashMap<String, String> diccionario = new HashMap<>();
        diccionario.put("Hello", "Hola");
        diccionario.put("Bye", "Adiós");
        diccionario.put("Day", "Día");
        diccionario.put("Leg", "Pierna");
        diccionario.put("Table", "Mesa");
        
        System.out.println("Escribe una palabra en inglés: ");
//        String palabra = lector.nextLine();
        
//                if (diccionario.containsKey(palabra)) {
//                    System.out.println(palabra + " - traducción -> " + diccionario.get(palabra));
//                }       
         
        System.out.println("");
        System.out.println("");
        System.out.println("");
         
        System.out.println("\t\t---- Ejercicio 3 ----");
        /*
        Crea una cola de supermercado. Aleatoriamente (70% probabilidades un cliente
        nuevo se pone en la cola) 30% un cliente de la cola ha sido atendido. Si la
        cola llega a tener 10 personas suena un mensaje por megafonía diciendo que,
        por favor, Pepito Pérez vuelva a su puesto de trabajo en Caja. Que se vea
        por consola todo el proceso (cada vez que alguien entra o sale de la cola
        y el mensaje por megafonía).
        */
        
        Stack<String> cola = new Stack<>();
        Random random = new Random();
        int numeroClientes = 0;
        
        while (cola.size() < 10) {            

            int numeroRandom = random.nextInt(11);
            
            if (numeroRandom < 7) {
                cola.push("Cliente");
                numeroClientes++;
                System.out.println(" - Cliente en la cola. Clientes: " + numeroClientes);
            
            } else if (!cola.isEmpty() && numeroRandom > 7) {
                cola.pop();
                numeroClientes--;
                System.out.println(" * Un cliente ha sido atendido. Clientes: " + numeroClientes);
            }

            if (cola.size() == 10) {
            System.out.println("Pepito Pérez, vuelva a su puesto de trabajo en caja.");    
            }   
        }   
    }    
}
